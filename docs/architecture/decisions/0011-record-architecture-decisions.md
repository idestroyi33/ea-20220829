# 1. Record architecture decisions

Date: 2023-04-10

## Status

Accepted

## Context

We need to record the architectural decisions made on this project.

## Decision 결정된 사항

We will use Architecture Decision Records, as [described by Michael Nygard](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions).

## Consequences 결정을 함으로써 무엇을 해야 하는지

See Michael Nygard's article, linked above. For a lightweight ADR toolset, see Nat Pryce's [adr-tools](https://github.com/npryce/adr-tools).
